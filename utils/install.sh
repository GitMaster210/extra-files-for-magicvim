#!/bin/bash

if [ -f "/etc/arch-release" ] || [ -f "/etc/artix-release" ]; then
	yes "" | sudo pacman -Sy git neovim xclip python3 python-pip nodejs npm gcc fzf ripgrep the_silver_searcher fd tree-sitter lazygit || exit 126
else # assume manual install
	exit
fi

pip install pynvim || exit 127

mkdir ~/.config/nvim
mkdir ~/.local/share/nvim

mv ~/.config/nvim ~/.config/nvim.bak || exit 127
mv ~/.local/share/nvim ~/.local/share/nvim.bak || exit 127

git clone https://gitlab.com/GitMaster210/magicvim ~/.config/nvim
nvim || exit 127
